import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import stocks from './modules/stocks';
import portfolio from './modules/portfolio';

Vue.use(Vuex);

// A store can either have its own state,getters,actions,mutations or it can have modules which then have their own parts
const store = new Vuex.Store({
	actions,
	modules:{
		stocks,
		portfolio
	}
});

export default store