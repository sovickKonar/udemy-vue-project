import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import store from './store/store';
import VueRouter from 'vue-router';
import { routes } from './routing/routes';

Vue.filter('currency',(value)=>{
	return '$'+ value.toLocaleString();
})

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.http.options.root = 'https://fir-vue-http-default-rtdb.firebaseio.com/';

const router = new VueRouter({
	mode:'history',
	routes
})


new Vue({
	router,
	store,
  render: h => h(App),
}).$mount('#app')
